ASM = nasm
ASMFLAGS = -f elf64 -o
LD = ld
LDFLAGS = -o

.PHONY = clean
.DEFAULT_GOAL = main

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) $@ $<

main.o: main.asm lib.o dict.o words.inc colon.inc
	$(ASM) $(ASMFLAGS) $@ $<

main: main.o lib.o dict.o
	$(LD) $(LDFLAGS) $@ $^

clean:
	rm lib.o main.o main
