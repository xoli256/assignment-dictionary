; macro for creating words in the dictionary

%define head 0

; key, label
%macro colon 2
    %ifid %2
        %2: dq head
        db %1, 0
        %define head %2
    %endif
%endmacro
