; provides _start function, which:
; - reads a string <= 255 symbols into buffer from stdin
; - searches the string in the dictionary
; - if success, prints result into stdout
; - otherwise prints error message into stderr

global _start

%include "words.inc"
extern read_line
extern print_string
extern print_error_string
extern print_newline
extern string_length
extern string_equals
extern exit
extern find_word

%define ADDR_LEN 8
%define BUF_LEN 256


section .rodata

quit_message: db "Quit", 0
exit_instruction: db "Type Quit to stop searching", 0
enter_invitation: db "Enter a word you are searching for: ", 0
word_not_found: db "Sorry, there is no such word in the dictionary", 0
key_too_long: db "Sorry, current dictionary version cannot handle keys > 255 symbols", 0
word_found: db "Explanation for word ", 0
semicolon: db ": ", 0


section .text

_start:
    mov rdi, exit_instruction
    call print_string
    call print_newline

.loop:
    mov rdi, enter_invitation
    call print_string

    sub rsp, BUF_LEN            ; word reading
    mov rdi, rsp
    mov rsi, BUF_LEN
    call read_line

    test rax, rax               ; chek if key is too long
    jz .key_too_long
    push rax

    mov rdi, rax                ; check if Quit command is entered
    mov rsi, quit_message
    call string_equals
    test rax, rax
    jnz .exit

    pop rax                     ; word finding
    mov rdi, rax
    mov rsi, head
    call find_word

    test rax, rax
    jz .not_found


    add rax, ADDR_LEN           ; printing key, word_found text
    mov rdi, rax
    push rdi
    mov rdi, word_found
    call print_string
    pop rdi
    push rdi
    call print_string
    mov rdi, semicolon
    call print_string
    pop rdi

    push rdi                    ; printing value
    call string_length
    pop rdi
    inc rdi
    add rdi, rax
    call print_string
    call print_newline
    add rsp, ADDR_LEN

    jmp .loop

.key_too_long:
    mov rdi, key_too_long
    call print_error_string
    call print_newline
    jmp .loop

.not_found:
    mov rdi, word_not_found
    call print_error_string
    call print_newline
    jmp .loop

.exit:
    call exit


