; provides find_word function, which:
; - takes as 1st argument a pointer to a null-terminated string
; - takes as 2nd argument a pointer to the dictionary stert
; - searches the string in the dictionary
; - if success, returns an address of the beginning of the entry into the dictionary
; - otherwise, returns 0

extern string_equals
global find_word

%define ADDR_LEN 8

section .text

find_word:
        ; pointer to null-terminated string in rdi
        ; pointer to dict start in rsi
        ; result in rax
.loop:
    push rdi
    push rsi
    add rsi, ADDR_LEN
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .end
    mov rsi, [rsi]
    test rsi, rsi
    jz .end
    jmp .loop
.end:
   mov rax, rsi
   ret